# app_configs



## Getting started

install argocd to cluster
```
kubectl create namespace argocd

helm repo add argo https://argoproj.github.io/argo-helm

helm install argocd argo/argo-cd --version 6.7.2 --namespace argocd

kubectl apply -n argocd -f applicationset.yaml
```

access argocd
```
kubectl port-forward service/argocd-server -n argocd 8080:443

kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

intall kubeseal

```commandline
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm install sealed-secrets -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets
```

install nginx controller
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```



## App launching

for using ingresses in minikube we should enable this option
```
minikube addons enable ingress
```

then we should get host name of our backend ingress and specify it for local resolving
```
kubectl get ingress -n <namespace>
```
![getting ingress info](./assets/img/get_ingress.png)

after we have got the host name, we should make some configurations in **/ets/hosts**
```
nano /etc/hosts
```
![hosts configuration](/assets/img/hosts_config.png)

get frontend url
```
minikube service <frontend-service-name> -n <namespace> --url
```
